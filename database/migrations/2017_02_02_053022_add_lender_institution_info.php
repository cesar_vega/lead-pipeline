<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLenderInstitutionInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lender_office', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('address1');
            $table->string('address2')->nullable();
            $table->string('city');
            $table->string('state');
            $table->string('postal');
            $table->integer('nmls_id');
            $table->timestamps();
        });
        
        Schema::table('lenders', function($table)
        {
            // Default 0 is so we can make column not nullable.
            $table->integer('nmls_id')->default(0);
            $table->integer('banker_id')->default(0);
            $table->integer('office_id')->default(0);
            
            // Add foreign restraint once data's been filled into table.
            //$table->foreign('lender_office_id')->references('id')->on('lender_office');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lenders', function($table)
        {
            // Add foreign restraint once data's been filled into table.
            // $table->dropForeign('lenders_lender_office_id_foreign');
            
            $table->dropColumn('nmls_id');
            $table->dropColumn('office_id');
        });
        
        Schema::drop('lender_office');
    }
}
