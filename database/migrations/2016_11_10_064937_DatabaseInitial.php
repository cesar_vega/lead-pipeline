<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DatabaseInitial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        
        Schema::create('lead_source', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('first');
            $table->string('last');
            $table->string('name');
            $table->string('number');
            $table->string('email');
            $table->string('title');
            $table->string('affiliation');
            $table->enum('preffered_contact', ['call', 'text', 'email']);
            $table->timestamps();
        });
        
        Schema::create('leads', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('lead_source_id');
            $table->foreign('lead_source_id')
                ->references('id')
                ->on('lead_source')
                ->onDelete('cascade');
            $table->string('first');
            $table->string('last');
            $table->string('name');
            $table->string('number');
            $table->string('email');
            $table->integer('income');
            $table->integer('debt');
            $table->boolean('verified');
            $table->integer('credit_score');
            $table->string('ssn');
            $table->enum('status', ['new', 'appointment', 'ready', 'remediate']);
            $table->timestamps();
        });
        
        Schema::create('lead_addresses', function(Blueprint $table)
        {            
            $table->increments('id');
            $table->integer('lead_id');
            $table->foreign('lead_id')
                ->references('id')
                ->on('leads')
                ->onDelete('cascade');
            $table->date('from');
            $table->date('to')->nullable();
            $table->string('line_one');
            $table->string('line_two')->nullable();
            $table->string('city');
            $table->string('state');
            $table->string('postal');
            $table->timestamps();
        });
        
        Schema::create('lenders', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('first');
            $table->string('last');
            $table->string('name');
            $table->string('number');
            $table->string('email');
            $table->timestamps();
        });
        
        Schema::create('appointments', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('with');
            $table->integer('for')->nullable();
            $table->datetime('when');
            $table->binary('completed');
            $table->timestamps();
            
            $table->foreign('for')->references('id')->on('leads')->onDelete('cascade');
            $table->foreign('with')->references('id')->on('lenders')->onDelete('cascade');
        });
        
        Schema::create('communications', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('person_id');
            $table->integer('template_id');
            $table->datetime('sent');
            $table->enum('direction', ['in', 'out']);
            $table->timestamps();
        });
        
        Schema::create('lead_notes', function (Blueprint $table)
        {
            $table->increments('id');
            
            $table->integer('lead_id');
            $table->foreign('lead_id')
                ->references('id')
                ->on('leads')
                ->onDelete('cascade');
            
            $table->integer('by');
            $table->text('note');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lead_notes');
        Schema::dropIfExists('communications');
        Schema::dropIfExists('appointments');
        Schema::dropIfExists('lead_addresses');
        Schema::dropIfExists('leads');
        Schema::dropIfExists('lead_source');
    }
}
