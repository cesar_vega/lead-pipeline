<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Appointment;
use App\Models\Lender;
use Carbon\Carbon;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $appointments = Appointment::query()->orderBy('when')->get();
        
        return view('appointments.index', compact('appointments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lenders = Lender::avaliableLenders();
        
        return view('appointments.create', compact('lenders'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $appointment = new Appointment;
        $appointment->for = $request->get('for');
        $appointment->with = $request->get('with');
        $appointment->when = $request->get('date') . ' ' . $request->get('time') . ':00';
        $appointment->completed = false;
        $appointment->save();
        
        return redirect()->route('appointment.show', ['id' => $appointment->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $appointment = Appointment::find($id);
        
        return view('appointments.show', compact('appointment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $appointment = Appointment::find($id);
        
        $appointment->when = $appointment->date = Carbon::parse($appointment->when);
        $appointment->time = $appointment->date->format('H:i');
        
        $lenders = Lender::avaliableLenders();
        
        return view('appointments.edit', compact('appointment', 'lenders'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
        $appointment = Appointment::find($id);
        $appointment->for = $request->get('for');
        $appointment->with = $request->get('with');
        $appointment->when = $request->get('date') . ' ' . $request->get('time') . ':00';
        $appointment->completed = false;
        $appointment->save();
        
        return redirect()->route('appointment.show', compact('id'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Appointment::find($id)->delete();
        
        return redirect()->route('appointment.index');
    }
}
