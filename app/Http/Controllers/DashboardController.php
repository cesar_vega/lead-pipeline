<?php namespace App\Http\Controllers;

use App\Models\LeadSource;

class DashboardController extends Controller
{
    public function getLeadSourceLeadCounts()
    {   
        return response()->json(LeadSource::getLeadCounts());
    }
}
