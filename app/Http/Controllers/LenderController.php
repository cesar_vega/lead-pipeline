<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Lender;
use App\Models\LenderOffice;

class LenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lenders = Lender::all();
        
        return view('lenders.index', compact('lenders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $offices = LenderOffice::getList();
        
        return view('lenders.create', compact('offices'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lender = new Lender($request->all());
        
        $lender->name = $lender->first . ' ' . $lender->last;
        $lender->save();
        
        return redirect()->route('lender.show', ['id' => $lender->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lender = Lender::find($id);
        
        return view('lenders.show', compact('lender'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lender = Lender::find($id);
        $offices = LenderOffice::getList();
        
        return view('lenders.edit', compact('lender', 'offices'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lender = Lender::find($id)->fill($request->all())->save();
        
        return redirect()->route('lender.show', compact('id'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Lender::find($id)->delete();
        
        return redirect()->route('lender.index');
    }
}
