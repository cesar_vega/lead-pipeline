<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\LeadSource;
use App\Models\Lender;
use App\Models\Appointment;
use App\Models\Lead;
use App\Jobs\QueuedText;

class LeadController extends Controller
{    
    public function dashboard()
    {       
       return redirect()->route('lead.index');

       // return opportunities
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leads = Lead::all();
        
        return view('leads.index', compact('leads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $leadSources = LeadSource::getList();
        $avaliableLenders = Lender::avaliableLenders();
        $appointments = Appointment::avaliable()->orderBy('when')->get();
        
        $context = compact('leadSources', 'avaliableLenders', 'appointments');
        
        return view('leads.create', $context);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lead = new Lead; 
        $lead->lead_source_id = $request->get('lead_source_id');
        $lead->first = $request->get('first');
        $lead->last = $request->get('last');
        $lead->name = $lead->first . ' ' . $lead->last;
        $lead->email = $request->get('email');
        $lead->number =  $request->get('phone_number');
        $lead->status = 'new';
        $lead->income = 0;
        $lead->debt = 0;
        $lead->verified = 0;
        $lead->credit_score = 0;
        $lead->ssn = 0;
        $lead->save();
        
        // Effectively, "fills" appointment.
        $appointment = Appointment::find($request->get('appointment_id'));
        $appointment->for = $lead->id;
        $appointment->save();
        
        $leadSource = $lead->source();
        $lender = $appointment->lender();
        $office = $lender->office();
        
        $context = compact('lead', 'leadSource', 'appointment', 'lender', 'office');
        
        for ($i = 1; $i < 5; $i++)
        {
            $leadText = new QueuedText($lead->number, 'leads.text' . $i, $context);
            $this->dispatch($leadText);
            $sourceText = new QueuedText($leadSource->number, 'leads.text' . $i, $context);
            $this->dispatch($sourceText);
            $lenderText = new QueuedText($lender->number, 'leads.text' . $i, $context);
            $this->dispatch($lenderText);
        }
        
        return redirect()->route('lead.thank-you');
    }
    
    public function thankYou()
    {
        return view('leads.thank-you');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lead = Lead::find($id);

        return view('leads.show', compact('lead'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lead = Lead::find($id);        
        $statuses = Lead::statuses();
                
        return view('leads.edit', compact('statuses', 'lead'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lead = Lead::find($id);
        $lead->status = $request->get('status');
        $lead->income = $request->get('income');
        $lead->debt = $request->get('debt');
        $lead->credit_score = $request->get('credit_score');
        $lead->verified = $request->get('verified');
        $lead->save();
        
        return redirect()->route('lead.edit', compact('id'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Lead::find($id)->delete();
        
        return redirect()->route('lead.index');
    }
}
