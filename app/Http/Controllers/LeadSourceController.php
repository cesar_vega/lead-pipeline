<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\LeadSource;

class LeadSourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leadSources = LeadSource::all();
        
        return view('lead_sources.index', compact('leadSources'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        return view('lead_sources.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $leadSource = new LeadSource($request->all());
        $leadSource->name = $leadSource->first . ' ' . $leadSource->last;
        $leadSource->preffered_contact = 'text';
        $leadSource->save();
        
        $id = $leadSource->id;
        return redirect()->route('lead-source.show', compact('id'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $leadSource = LeadSource::find($id);
        
        return view('lead_sources.show', compact('leadSource'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $leadSource = LeadSource::find($id);
        
        return view('lead_sources.edit', compact('leadSource'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $leadSource = LeadSource::find($id);
        $leadSource->fill($request->all());
        $leadSource->save();
        
        return redirect()->route('lead-source.show', compact('id'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        LeadSource::find($id)->delete();
        
        return redirect()->route('lead-source.index');
    }
}
