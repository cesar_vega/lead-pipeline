<?php

Route::get(
    '/', function () {
        return redirect()->route('lead.index');
    }
);

Route::get('dashboard', function() 
{
    return view('dashboard.index');
    
})->name('dashboard');

Route::resource('lead-source', 'LeadSourceController');
Route::resource('lender', 'LenderController');
Route::resource('lender-office', 'LenderOfficeController');
Route::resource('lead-address', 'LeadAddressController');
Route::resource('appointment', 'AppointmentController');

Route::get('lead/thank-you', [
    'uses' => 'LeadController@thankYou',
    'as' => 'lead.thank-you'
]);
Route::resource('lead', 'LeadController');

Route::get('dashboard/lead-counts', [
    'uses' => 'DashboardController@getLeadSourceLeadCounts',
    'as' => 'dashboard.lead-counts',
]);

Route::get(
    'lead/summary-by-lead-source',
    ['as' => 'lead.summary_by_lead_source',
     'uses' => 'LeadController@summaryByLeadSource'
    ]
);

Route::get(
    'lead/summary-by-city',
    ['as' => 'lead.summary_by_city',
     'uses' => 'LeadController@summaryByCity'
    ]
);