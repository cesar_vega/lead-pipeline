<?php
namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Phone;

class QueuedText extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels, Phone;

    /**
     * Number we're sending a message too.
     * 
     * @var string
     */
    protected $number;
    
    /**
     * Text template you'd like you send.
     * 
     * @var string
     */
    protected $template;
    
    /**
     * Message context.
     * 
     * @var array
     */
    protected $context;
    
    
    /**
     * 
     * @param string $number Number you'd like to send a text too.
     * @param string $template Text template you'd like you send.
     * @param array $context Message context.
     */
    public function __construct($number, $template, $context)
    {
        $this->number = $number;
        $this->template = $template;
        $this->context = $context;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->twilio()->messages->create($this->number, [
            'from' => env('FROM_NUMBER'),
            'body' => view($this->template, $this->context)->render()
        ]);
        
        // This is to ensure messages get sent in order.  Shorter messages tend
        // to go out quicker.  Even if it was sent to the API first.
        sleep(3);  
    }
}
