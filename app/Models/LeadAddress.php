<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeadAddress extends Model
{
    protected $table = 'lead_addresses';
    
    protected $guarded = [];
}
