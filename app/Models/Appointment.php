<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Lender;
use App\Models\Lead;
use Carbon\Carbon;

class Appointment extends Model
{
    protected $table = 'appointments';
    
    protected $guarded = [];
    
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'when'
    ];
    
    public function lender()
    {
        return $this->hasOne(Lender::class, 'id', 'with')->first();
    }
    
    public function lead()
    {
        return $this->hasOne(Lead::class, 'id', 'for')->firstOrNew([]);
    }
    
    public static function avaliable()
    {
        return static::query()->where('when', '>=', Carbon::now())
                ->whereNull('for');
    }
}
