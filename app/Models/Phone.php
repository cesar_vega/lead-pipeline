<?php namespace App\Models;

use Twilio\Rest\Client;

trait Phone
{
    protected $twilio;
    
    protected function twilio()
    {
        if ( ! isset($this->twilio))
        {
            $this->twilio = \App::make(Client::class);
        }
        
        return $this->twilio;
    }
    
    /**
     * 
     * @param type $template
     * @param type $context
     */
    public function sendText($template, $context)
    {
        $this->twilio()->messages->create('+1' . $this->number, [
            'from' => env('FROM_NUMBER'),
            'body' => view($template, $context)->render()
        ]);
    }
}
