<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\LeadSource;
use App\Models\LeadAddress;

class Lead extends Model
{    
    use Phone;
    
    public function source()
    {
        return $this->hasOne(LeadSource::class, 'id', 'lead_source_id')
                ->first();
    }
    
    public function addresses()
    {
        return $this->hasMany(LeadAddress::class, 'lead_id', 'id');
    }
    
    public static function statuses()
    {
        return [
            'new' => 'New Lead',
            'confirmed' => 'Appointment Confirmed',
            'ready' => 'Ready for Loan',
            'remediate' => 'In Remediation',
        ];
    }
}
