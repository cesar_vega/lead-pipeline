<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LenderOffice extends Model
{
    protected $table = 'lender_office';
    
    protected $guarded = [];
    
    public static function getList()
    {
        $offices = static::all();
        $list = [];
        
        foreach ($offices as $office)
        {
            $list[$office->id] = $office->name;
        }
        
        return $list;
    }
}
