<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\LenderOffice;

class Lender extends Model
{
    use Phone;
    
    protected $table = 'lenders';
    
    protected $guarded = [];
    
    public function office()
    {
        return $this->hasOne(LenderOffice::class, 'id', 'office_id')->first();
    }
    
    public static function avaliableLenders()
    {
        $lenders = static::all();
        $list = [];
        
        foreach ($lenders as $lender)
        {
            $list[$lender->id] = $lender->name;
        }
        
        return $list;
    }
}
