<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class LeadSource extends Model
{
    use Phone;
    
    protected $table = 'lead_source';
    
    protected $guarded = [];
    
    public static function getList()
    {
        $leads = static::all();
        $list = [];
        
        foreach ($leads as $lead)
        {
            $list[$lead->id] = $lead->name;
        }
        
        return $list;
    }
    
    /**
     * Get the amount of leads each source provides
     * 
     * @return array
     */
    public static function getLeadCounts()
    {
        return DB::table('leads')
               ->join('lead_source', 'leads.lead_source_id', '=', 'lead_source.id')
               ->select(DB::raw('count(*) as count'),'lead_source.name')
               ->groupBy('lead_source.name')
               ->get();
    }
}
