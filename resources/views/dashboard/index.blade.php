@extends('layouts.master')

@section('content')
<h3>Leads by Lead Source</h3>
<p>Who's bringing in the most leads</p>
<canvas id="lead-counts-pie"></canvas>
@stop

@section('scripts')
    {!! Html::script('//cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js') !!}
    {!! Html::script('//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js') !!}
    {!! Html::script('/js/pieCharts.js') !!}
@stop