@extends('layouts.master')

@section('content')
    <h3>{{ $lender->id }}</h3>
    <p>First: {{ $lender->first }}</p>
    <p>Last: {{ $lender->last }}</p>
    <p>Email: {{ $lender->email }}</p>
    <p>Number: {{ $lender->number }}</p>
    <a href='{{ route('lender.edit', ['id' => $lender->id]) }}'>Edit</a>
@stop
