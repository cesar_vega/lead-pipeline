@extends('layouts.master')

@section('content')
    <h3>Lenders</h3>
    <div class="row">
        <div class="col-lg-8">
            <table class="table">
                <thead>
                    <th>Name</th>
                    <th>
                      <a href="{{ route('lender.create') }}" class="btn btn-primary btn-xs">
                        Add
                      </a>
                    </th>
                </thead>
                <tbody>
                    @foreach ($lenders as $lender)
                        <tr>
                            <td> {{ $lender->name }} </td>
                            <td>
                                {!! Html::link(route('lender.edit', $lender->id), 'Edit',
                                               ['class' => 'btn btn-default btn-xs']) !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop