@extends('layouts.master')

@section('content')
    <h1>Add new Lender</h1>
    <hr>
        {!! Form::open(['route' => 'lender.store']) !!}
        <div class="form-group">
            {!! Form::label('first', 'First Name:') !!}
            {!! Form::text('first') !!}
        </div>
        <div class="form-group">
            {!! Form::label('last', 'Last Name:') !!}
            {!! Form::text('last') !!}
        </div>
        <div class="form-group">
            {!! Form::label('nmls_id', 'National Mortgage Lender ID:') !!}
            {!! Form::text('nmls_id') !!}
        </div>
        <div class="form-group">
            {!! Form::label('banker_id', 'Banker License Number:') !!}
            {!! Form::text('banker_id') !!}
        </div>
        <div class="form-group">
            {!! Form::label('office_id', 'Office:') !!}
            {!! Form::select('office_id', $offices) !!}
        </div>
        <div class="form-group">
            {!! Form::label('number', 'Phone Number:') !!}
            {!! Form::text('number') !!}
        </div>
        <div class="form-group">
            {!! Form::label('email', 'Email:') !!}
            {!! Form::text('email') !!}
        </div>
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}
@stop

