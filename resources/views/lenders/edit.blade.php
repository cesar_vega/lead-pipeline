@extends('layouts.master')

@section('content')
    <h1>Edit Lender</h1>
    <hr>
        {!! Form::model($lender, [
        'route' => ['lender.update', $lender->id], 
        'method' => 'PUT'
        ]) !!}
        <div class="form-group">
            {!! Form::label('first', 'First:') !!}
            {!! Form::text('first') !!}
        </div>
        <div class="form-group">
            {!! Form::label('last', 'Last:') !!}
            {!! Form::text('last') !!}
        </div>
        <div class="form-group">
            {!! Form::label('nmls_id', 'National Mortgage Lender ID:') !!}
            {!! Form::text('nmls_id') !!}
        </div>
        <div class="form-group">
            {!! Form::label('banker_id', 'Banker License Number:') !!}
            {!! Form::text('banker_id') !!}
        </div>
        <div class="form-group">
            {!! Form::label('office_id', 'Office:') !!}
            {!! Form::select('office_id', $offices, $lender->office_id) !!}
        </div>
        <div class="form-group">
            {!! Form::label('number', 'Phone:') !!}
            {!! Form::text('number') !!}
        </div>
        <div class="form-group">
            {!! Form::label('email', 'Email:') !!}
            {!! Form::text('email') !!}
        </div>
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}

    {!! Form::open(['url' => route('lender.destroy', $lender->id),
                    'method' => 'DELETE']) !!}
    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
    {!! Form::close() !!}
@stop
