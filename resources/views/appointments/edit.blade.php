@extends('layouts.master')

@section('content')
    <h2>Edit Appointment</h2>
    {!! Form::open(['url' => route('appointment.destroy', $appointment->id),
                'method' => 'delete']) !!}
    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
    {!! Form::close() !!}
    {!! Form::model($appointment, [
        'route' => ['appointment.update', $appointment->id], 
        'method' => 'put'
    ]) !!}
    <div class="form-group">
        {!! Form::label('with', 'With:') !!}
        {!! Form::select('with', $lenders, $appointment->lender()->id) !!}
    </div>
    <div class="form-group">
        {!! Form::label('date', 'When:') !!}
        {!! Form::date('date', $appointment->date) !!}
        {!! Form::time('time', $appointment->time) !!}
    </div>

    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}
@stop