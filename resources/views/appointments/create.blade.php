@extends('layouts.master')

@section('content')
    <h2>Add a new appointment slot</h2>
    <hr />
    {!! Form::open(['route' => 'appointment.store']) !!}
    <div class="form-group">
        {!! Form::label('with', 'With:') !!}
        {!! Form::select('with', $lenders) !!}
    </div>
    <div class="form-group">
        {!! Form::label('date', 'When:') !!}
        {!! Form::date('date') !!}
        {!! Form::time('time') !!}
    </div>
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}
@stop

