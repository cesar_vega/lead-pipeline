@extends('layouts.master')

@section('content')
    <h3>Appointments</h3>
    <div class="row">
        <div class="col-lg-8">
            <table class="table">
                <thead>
                    <th>For</th>
                    <th>With</th>
                    <th>When</th>
                    <th>Number</th>
                    <th>
                      <a href="{{ route('appointment.create') }}" class="btn btn-primary btn-xs">
                        Add
                      </a>
                    </th>
                </thead>
                <tbody>
                    @foreach ($appointments as $appointment)
                        <tr>
                            <td> {{ $appointment->lead()->name }} </td>
                            <td> {{ $appointment->lender()->name }} </td>
                            <td> {{ $appointment->when->format('m/d - g:ia (l)') }} </td>
                            <td> {{ $appointment->lead()->number }} </td>
                            <td>
                                {!! Html::link(route('appointment.edit', $appointment->id), 'Edit',
                                               ['class' => 'btn btn-default btn-xs']) !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

