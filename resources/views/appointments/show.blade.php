@extends('layouts.master')

@section('content')
    <p>For: {{ ($lead = $appointment->lead()) !== null ? $lead->name : '' }}</p>
    <p>With: {{ $appointment->lender()->name }}</p>
    <p>When: {{ $appointment->when->format('F jS - g:ia (l)') }}</p>
    <a href='{{ route('appointment.edit', ['id' => $appointment->id]) }}'>Edit</a>
@stop
