@extends('layouts.master')

@section('content')
    <h3>Edit Mortgage Office</h3>
    <hr>
    {!! Form::model($office, [
    'route' => ['lender-office.update', $office->id], 
    'method' => 'PUT'
    ]) !!}
        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name') !!}
        </div>
        <div class="form-group">
            {!! Form::label('nmls_id', 'National Mortgage Lender ID:') !!}
            {!! Form::text('nmls_id') !!}
        </div>
        <div class="form-group">
            {!! Form::label('address1', 'Address 1:') !!}
            {!! Form::text('address1') !!}
        </div>
        <div class="form-group">
            {!! Form::label('address2', 'Address 2:') !!}
            {!! Form::text('address2') !!}
        </div>
        <div class="form-group">
            {!! Form::label('city', 'City:') !!}
            {!! Form::text('city') !!}
        </div>
        <div class="form-group">
            {!! Form::label('state', 'State:') !!}
            {!! Form::select('state', states()) !!}
        </div>
        <div class="form-group">
            {!! Form::label('postal', 'Zip Code:') !!}
            {!! Form::text('postal') !!}
        </div>
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    {!! Form::close() !!}

    {!! Form::open(['url' => route('lender-office.destroy', $office->id),
                    'method' => 'DELETE']) !!}
        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
    {!! Form::close() !!}
@stop
