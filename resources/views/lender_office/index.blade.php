@extends('layouts.master')

@section('content')
    <h3>Mortgage Offices</h3>
    <div class="row">
        <div class="col-lg-8">
            <table class="table">
                <thead>
                    <th>Name</th>
                    <th>
                      <a href="{{ route('lender-office.create') }}" class="btn btn-primary btn-xs">
                        Add
                      </a>
                    </th>
                </thead>
                <tbody>
                    @foreach ($offices as $office)
                        <tr>
                            <td> {{ $office->name }} </td>
                            <td>
                                {!! Html::link(route('lender-office.edit', $office->id), 'Edit',
                                               ['class' => 'btn btn-default btn-xs']) !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop