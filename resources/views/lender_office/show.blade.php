@extends('layouts.master')

@section('content')
    <h3>{{ $office->name }}</h3>
    <p>National Mortgage Lender ID: {{ $office->nmls_id }}</p>
    <p>Address 1: {{ $office->address1 }}</p>
    <p>Address 2: {{ $office->address2 }}</p>
    <p>City: {{ $office->city }}</p>
    <p>State: {{ $office->state }}</p>
    <p>Postal: {{ $office->postal }}</p>
    <a href='{{ route('lender-office.edit', ['id' => $office->id]) }}'>Edit</a>
@stop
