@extends('layouts.master')

@section('content')
    <h1>Edit a lead source</h1>
    <hr>
    <h3>{{$leadSource->name}}</h3>
            <hr>
        {!! Form::open(['url' => route('lead-source.update', ['id' => $leadSource->id]), 
                        'method' => 'put']) !!}
        <div class="form-group">
            {!! Form::label('title', 'Title:') !!}
            {!! Form::text('title', $leadSource->title) !!}
        </div>
        <div class="form-group">
            {!! Form::label('affiliation', 'Affiliation:') !!}
            {!! Form::text('affiliation', $leadSource->affiliation) !!}
        </div>
        <div class="form-group">
            {!! Form::label('number', 'Phone Number:') !!}
            {!! Form::text('number', $leadSource->number) !!}
        </div>
        <div class="form-group">
            {!! Form::label('email', 'E-mail:') !!}
            {!! Form::text('email', $leadSource->email) !!}
        </div>
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}

    {!! Form::open(['url' => route('lead-source.destroy', $leadSource->id),
                    'method' => 'DELETE']) !!}
    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
    {!! Form::close() !!}
@stop
