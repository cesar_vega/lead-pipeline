@extends('layouts.master')

@section('content')
<h3>{{$leadSource->name}}</h3>
<p>Title: {{$leadSource->title}}</p>
<p>Affiliation: {{$leadSource->affiliation}}</p>
<p>Number: {{$leadSource->number}}</p>
<a href='{{route('lead-source.edit', ['id' => $leadSource->id])}}'>Edit</a>
@stop
