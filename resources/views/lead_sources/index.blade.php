@extends('layouts.master')

@section('content')
<h3>Lead sources</h3>
<div class="row">
    <div class="col-lg-8">
        <table class="table">
            <thead>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Title</th>
                <th>Affiliation</th>
                <th>
                  <a href="{{ route('lead-source.create') }}" class="btn btn-primary btn-xs">
                    Add
                  </a>
                </th>
            </thead>
            <tbody>
                @foreach ($leadSources as $leadSource)
                    <tr>
                        <td> {{ $leadSource->first }} </td>
                        <td> {{ $leadSource->last }} </td>
                        <td> {{ $leadSource->title }} </td>
                        <td> {{ $leadSource->affiliation }} </td>
                        <td>
                            {!! Html::link(route('lead-source.edit', $leadSource->id), 'Edit',
                                           ['class' => 'btn btn-default btn-xs']) !!}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop