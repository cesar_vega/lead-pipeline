@extends('layouts.master')

@section('content')
    <h1>Create a new lead source</h1>
    <hr>
        {!! Form::open(['route' => 'lead-source.store']) !!}
        <div class="form-group">
            {!! Form::label('first', 'First Name:') !!}
            {!! Form::text('first') !!}
        </div>
        <div class="form-group">
            {!! Form::label('last', 'Last Name:') !!}
            {!! Form::text('last') !!}
        </div>
        <div class="form-group">
            {!! Form::label('number', 'Phone Number:') !!}
            {!! Form::text('number') !!}
        </div>
        <div class="form-group">
            {!! Form::label('email', 'Email:') !!}
            {!! Form::text('email') !!}
        </div>
                <div class="form-group">
            {!! Form::label('title', 'Title:') !!}
            {!! Form::text('title') !!}
        </div>
        <div class="form-group">
            {!! Form::label('affiliation', 'Affiliation:') !!}
            {!! Form::text('affiliation') !!}
        </div>
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}
@stop

