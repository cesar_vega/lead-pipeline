<!doctype html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="{{ asset('css/main.css') }}" rel="stylesheet">
        <title>@yield('title', 'Lead Pipeline')</title>
    </head>
    <body>
        <div class="container-fluid">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <a class="navbar-brand" href="/">Lead Pipeline</a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span> 
                </button>
            </div>
            <div class="collapse navbar-collapse" id="main-navbar">
                <ul class="nav navbar-nav">
                    <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                </ul>
                <ul class="nav navbar-nav">
                    <li><a href="{{ route('lead.index') }}">Leads</a></li>
                </ul>
                <ul class="nav navbar-nav">
                      <li><a href="{{ route('lead-source.index') }}">Lead Sources</a></li>
                </ul>
                <ul class="nav navbar-nav">
                      <li><a href="{{ route('lender.index') }}">Lenders</a></li>
                </ul>
                <ul class="nav navbar-nav">
                      <li><a href="{{ route('lender-office.index') }}">Mortgage Offices</a></li>
                </ul>
                <ul class="nav navbar-nav">
                      <li><a href="{{ route('appointment.index') }}">Appointments</a></li>
                </ul>
            </div>
        </nav>
        <div class="errors">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        @yield('content')
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        @yield('scripts')
        </div>
    </body>
</html>
