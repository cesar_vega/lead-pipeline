@extends('layouts.master')

@section('content')
    <h2>Edit a lead </h2>
    
    <h3>{{ $lead->name }}</h3>
    <h4>Referred by: {{ $lead->source()->name }}</h4>
        {!! Form::model($lead, [
            'route' => ['lead.update', $lead->id], 
            'method' => 'put'
        ]) !!}
        <div class="form-group">
            {!! Form::label('status', 'Status:') !!}
            {!! Form::select('status', $statuses, $lead->status) !!}
        </div>
        <div class="form-group">
            {!! Form::label('income', 'Income:') !!}
            {!! Form::text('income', $lead->income) !!}
        </div>
        <div class="form-group">
            {!! Form::label('debt', 'Debt:') !!}
            {!! Form::text('debt', $lead->debt) !!}
        </div>
        <div class="form-group">
            {!! Form::label('credit_score', 'Credit Score:') !!}
            {!! Form::text('credit_score', $lead->credit_score) !!}
        </div>
        <div class="form-group">
            {!! Form::label('verified', 'Verified:') !!}
            {!! Form::select('verified', [0 => 'No', 1 => 'Yes'], 
            $lead->verified) !!}
        </div>
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}
        
        <hr />
        
        <h3>Addresses</h3>
        
        <h4>Current Addresses</h4>
        <div class="row">
            <div class="col-lg-8">
                <table class="table">
                    <thead>
                        <th>Address</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Postal</th>
                        <th></th>
                    </thead>
                    <tbody>
                        @foreach ($lead->addresses()->get() as $address)
                            <tr>
                                <td>{{ $address->line_one }} {{ $address->line_two }}</td>
                                <td>{{ $address->city }}</td>
                                <td>{{ $address->state }}</td>
                                <td>{{ $address->postal }}</td>
                                <td>
                                    {!! Html::link(route('lead-address.edit', $address->id),
                                    'Edit', ['class' => 'btn btn-default btn-xs']) !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        
        <h4>Add address</h4>
        {!! Form::open(['route' => 'lead-address.store']) !!}
        {!! Form::hidden('lead_id', $lead->id) !!}
        <div class="form-group">
            {!! Form::label('from', 'From:') !!}
            {!! Form::date('from') !!}
        </div>
        <div class="form-group">
            {!! Form::label('to', 'To:') !!}
            {!! Form::date('to') !!}
        </div>
        <div class="form-group">
            {!! Form::label('line_one', 'Address 1:') !!}
            {!! Form::text('line_one') !!}
        </div>
        <div class="form-group">
            {!! Form::label('line_two', 'Address 2:') !!}
            {!! Form::text('line_two') !!}
        </div>
        <div class="form-group">
            {!! Form::label('city', 'City:') !!}
            {!! Form::text('city') !!}
        </div>
        <div class="form-group">
            {!! Form::label('state', 'State:') !!}
            {!! Form::text('state') !!}
        </div>
        <div class="form-group">
            {!! Form::label('postal', 'Postal:') !!}
            {!! Form::text('postal') !!}
        </div>
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}
        
        <hr />
        {!! Form::open(['url' => route('lead.destroy', $lead->id),
                    'method' => 'DELETE']) !!}
        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
        {!! Form::close() !!}
@stop
