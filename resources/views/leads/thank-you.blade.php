@extends('layouts.temp')

@section('content')
<div id="page"></div>
@stop

@section('scripts')
<script>
$( document ).ready(function() {
    var count = 5;
    var countdown = setInterval(function(){
      $("#page").html(
        "<h3>Thank you</h3>" + 
        "<h4>We'll take you back in " + count + " seconds.</h4>"
      );
      if (count === 0) {
        clearInterval(countdown);
        window.open("{{ route('lead.create') }}", "_self");
      }
      count--;
    }, 1000);
});
</script>
@stop