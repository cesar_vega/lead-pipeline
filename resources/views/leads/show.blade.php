@extends('layouts.master')

@section('content')
    <h3>{{ $lead->name }}</h3>
    <p>Source: {{ $lead->source()->name }}</p>
    <p>Status: {{ $lead->status }}</p>
    <p>Income: {{ $lead->income }}</p>
    <p>Debt: {{ $lead->debt }}</p>
    <p>Verified: {{ $lead->verified == 1 ? "Y" : "N" }}</p>
    <p>Credit Score: {{ $lead->credit_score }}</p>
    <a href='{{ route('lead.edit', ['id' => $lead->id]) }}'>Edit</a>
@stop
