@extends('layouts.master')

@section('content')    
    <div class="row">
        <div class="col-lg-8">
          
            <table class="table">
                <thead>
                    <th>Name</th>
                    <th>
                      <a href="{{ route('lead.create') }}" class="btn btn-primary btn-xs">
                        Add
                      </a>
                    </th>
                </thead>
                <tbody>
                    @if (count($leads) > 0)
                        @foreach ($leads as $lead)
                            <tr>
                                <td> {{ $lead->name }} </td>
                                <td>
                                    {!! Html::link(route('lead.edit', $lead->id), 'Edit',
                                                   ['class' => 'btn btn-default btn-xs']) !!}
                                </td>
                            </tr>
                        @endforeach
                    @else
                    <td>Let's get some leads!</td>
                    @endif
                </tbody>
            </table>
            
        </div>
    </div>
@stop
