@extends('layouts.temp')

@section('content')
    <h1>Create a new lead</h1>
    <hr>
        {!! Form::open(['route' => 'lead.store']) !!}
        <div class="form-group">
            {!! Form::label('first', 'First Name:') !!}
            {!! Form::text('first') !!}
        </div>
        <div class="form-group">
            {!! Form::label('last', 'Last Name:') !!}
            {!! Form::text('last') !!}
        </div>
        <div class="form-group">
            {!! Form::label('phone_number', 'Phone Number:') !!}
            {!! Form::text('phone_number') !!}
        </div>
        <div class="form-group">
            {!! Form::label('email', 'Email:') !!}
            {!! Form::text('email') !!}
        </div>
        <div class="form-group">
            {!! Form::label('lead_source_id', 'Lead Source:') !!}
            {!! Form::select('lead_source_id', $leadSources) !!}
        </div>
        <h3>Avaliable Appointments</h3>
        <div class="row">
            <div class="col-lg-8">
                <table class="table">
                    <thead>
                        <th>With</th>
                        <th>When</th>
                        <th></th>
                    </thead>
                    <tbody>
                        @foreach ($appointments as $appointment)
                            <tr>
                                <td> {{ $appointment->lender()->name }} </td>
                                <td> {{ $appointment->when->format('F jS - g:ia (l)') }} </td>
                                <td>
                                  {!! Form::radio('appointment_id', $appointment->id) !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
    </div>
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}
@stop

