{{ $lead->name }}'s appointment is set!

They're seeing {{ $lender->first }} on {{ $appointment->when->format('n/j') }} @ {{ $appointment->when->format('g:ia') }}.

Contact Info:
{{ $lead->number }}
{{ $lead->email }}

- Pipeline