You're all set!

You are seeing {{ $lender->name }} on {{ $appointment->when->format('n/j') }} @ {{ $appointment->when->format('g:ia') }}.

Thank you,
{{ $leadSource->name }}
{{ $leadSource->number }}