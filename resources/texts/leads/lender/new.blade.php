New lead: {{ $lead->name }}

{{ $appointment->when->format('n/j') }} @ {{ $appointment->when->format('g:ia') }}.

{{ $lead->number }}
{{ $lead->email }}

- Pipeline